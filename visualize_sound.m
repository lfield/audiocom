%VISUALIZE_SOUND visualizes a sound file for a given time interval
%
%   Example use:
%   >> clear all; clear functions; close all; cd Audiocom/audio_files/;visualize_sound('MobileComp2_L',[0 10],[0 10])
%
%   Stefan Mangold

function visualize_sound(aWavFile,time_interval_s, freq_interval_Hz)
  %VISUALIZE_SOUND visualizes a sound file for a given time interval
  %
  %   Example use:
  %   >> clear all; clear functions; close all; cd Audiocom/audio_files/;visualize_sound('MobileComp2_L',[0 10],[0 10])

  warning ("off", "Octave:future-time-stamp")
  pkg load signal;

  [x,fs] = audioread(aWavFile);   % get a section of the sound file
  time_interval_samples = round(time_interval_s * fs);

  if (min(time_interval_s)==0)
    time_interval_s(1) = time_interval_s(1) + 0.001;
  end
  time_interval_samples = round(time_interval_s * fs);

  [x,fs] = audioread([aWavFile], time_interval_samples);

  x = x(:,1); % get the first channel
%  xmax = max(abs(x)); % find the maximum value
%  x = x/xmax; % scaling the signal

  N = length(x); % time & discretization parameters
  t = time_interval_s(1) + (0:N-1)/fs;

  %% -- simple graph over time -------------------------------------------------------------------------------
  aLineWidth=2;
  FontSize=12;
  aMarkerSize=7;

  figName = ([ aWavFile '_time_domain']);
  figure('NumberTitle', 'on', 'Name', figName,'PaperPositionMode', 'auto','Position', [100 560 700 300],'Pointer', 'arrow');
  box on; set(gcf, 'color', 'white'); set(gcf, 'InvertHardCopy', 'off'); set(gca,'Position',[0.1,0.14,0.88,0.82]);
  plot(t,abs(x));
  xlim(time_interval_s);
  ylim([0 1.05*max(abs(x))]);
  grid on;
  xlabel('Time [s]');
  ylabel('Magnitude');
  set(gca,'FontSize',FontSize);
  set(gca,'FontName','Roboto');

  %% -- simple graph over freq -------------------------------------------------------------------------------
  figName = ([aWavFile '_freq_domain']);
  figure('NumberTitle', 'on', 'Name', figName,'PaperPositionMode', 'auto','Position', [820 560 700 300],'Pointer', 'arrow');
  box on; axis on; set(gcf, 'color', 'white'); set(gcf, 'InvertHardCopy', 'off'); set(gca,'Position',[0.1,0.14,0.88,0.82]);
  % FFT

  win = hanning(N);
  K = sum(win)/N; % coherent amplification of the window
  X = abs(fft(x.*win)); % fast fourier transform
  Xm = X(1:round(N/2)); % getting a first half of the spectrum without Nyquist frequency at N/2+1
  Xm = Xm/(round(N/2)); % computing of the amplitudes
  Xm(1,1) = Xm(1,1)/2; % correction of the DC component
  Xm = Xm/K; % correction due to coherent amplification
  % plotting of the spectrum
  f = (0:round(N/2)-1)*fs/N;
  plot(f/1000, 20*log10(Xm))
  xlim(freq_interval_Hz/1000);
  grid on
  xlabel('Frequency [kHz]')
  ylabel('Amplitude [dB]')
  set(gca,'FontSize',FontSize);
  set(gca,'FontName','Roboto');



  %% -- re-read audiofile --------------------------------------------------------------------------

  [x, fs] = audioread(aWavFile, time_interval_samples);  % Load the audio signal
  nfft = 256;  % FFT size
  window = hanning(nfft);  % Window function
  noverlap = nfft / 2;  % Number of overlapping samples
  [S, f, t] = specgram(x, nfft, fs, window, noverlap);  % Compute spectrogram

  %% -- 2d graph of magnitude --------------------------------------------------------------------------

  figName = ([ aWavFile '_spectrogram']);
  figure('NumberTitle', 'on', 'Name', figName,'PaperPositionMode', 'auto','Position', [100 100 700 300],'Pointer', 'arrow');
  set(gcf, 'color', 'white'); set(gcf, 'InvertHardCopy', 'off'); set(gca,'Position',[0.09,0.14,0.84,0.82]);

%  specgram(x,nfft,fs)

  imagesc(t, f, abs(S));  % Plot the magnitude
  colormap("pink");

  axis xy;  % Set the correct orientation
  xlabel('Time [s]')
  ylabel('Frequency [kHz]')
  box on; h = colorbar;
  ylabel(h, 'Magnitude [dB]');
  xlim(time_interval_s);
  ylim(freq_interval_Hz);

  set(gca,'YTickLabel',get(gca,'YTick')/1000);
  set(gca,'FontSize',FontSize);
  set(gca,'FontName','Roboto');


  %% -- re-read audiofile --------------------------------------------------------------------------

  [x, fs] = audioread(aWavFile, time_interval_samples);  % Load the audio signal
  nfft = 256;  % FFT size
  window = hanning(nfft);  % Window function
  noverlap = nfft / 2;  % Number of overlapping samples
  [S, f, t] = specgram(x, nfft, fs, window, noverlap);  % Compute spectrogram

  %% -- 2d graph of phases --------------------------------------------------------------------------

  figName = ([ aWavFile '_spectrogram_phases']);
  figure('NumberTitle', 'on', 'Name', figName,'PaperPositionMode', 'auto','Position', [820 100 700 300],'Pointer', 'arrow');
  set(gcf, 'color', 'white'); set(gcf, 'InvertHardCopy', 'off'); set(gca,'Position',[0.09,0.14,0.84,0.82]);

  phase = angle(S);  % Get the phase of each element in the spectrogram

  imagesc(t, f, phase);  % Plot the phase
  axis xy;  % Set the correct orientation
  xlabel('Time (s)');
  ylabel('Frequency (kHz)');

  xlim(time_interval_s);
  ylim(freq_interval_Hz);

  % Define a custom colormap
  n = 64;  % Number of color levels
  yellow = [.9, .9, .9];  % RGB values for yellow
  blue = [.1, .1, .9];    % RGB values for blue
  half_n = floor(n / 2);
  colormap1 = [linspace(yellow(1), blue(1), half_n)', linspace(yellow(2), blue(2), half_n)', linspace(yellow(3), blue(3), half_n)'];
  colormap2 = [linspace(blue(1), yellow(1), half_n)', linspace(blue(2), yellow(2), half_n)', linspace(blue(3), yellow(3), half_n)'];
  custom_colormap = [colormap1; colormap2];
  colormap(custom_colormap);

  box on; h = colorbar;
  ylabel(h, 'Phase');
  set(gca,'YTickLabel',get(gca,'YTick')/1000);
  set(gca,'FontSize',FontSize);
  set(gca,'FontName','Roboto');

endfunction
